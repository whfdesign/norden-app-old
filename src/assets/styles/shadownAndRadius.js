import colors from '~/assets/styles/colors';

export default {
  borderRadius2: {
    borderRadius: 2,
  },
  borderRadius4: {
    borderRadius: 4,
  },
  borderRadius6: {
    borderRadius: 6,
  },
  borderRadius12: {
    borderRadius: 12,
  },
  shadow100: {
    shadowColor: colors.NORDEN_DARK_BLUE,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 1,
    shadowOpacity: 0.25,
  },
  shadow200: {
    shadowColor: colors.NORDEN_DARK_BLUE,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 4,
    shadowOpacity: 0.2,
  },
  shadow300: {
    shadowColor: colors.NORDEN_DARK_BLUE,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowRadius: 12,
    shadowOpacity: 0.18,
  },
  shadow400: {
    shadowColor: colors.NORDEN_DARK_BLUE,
    shadowOffset: {
      width: 0,
      height: 9,
    },
    shadowRadius: 22,
    shadowOpacity: 0.18,
  },
};
