import AsyncStorage from '@react-native-community/async-storage';
import axios from 'react-native-axios';

const HOST_APP = 'https://demo9839409.mockable.io';

export {HOST_APP};

export default {
  default: {
    client: axios.create({
      baseURL: HOST_APP,
      responseType: 'json',
    }),
    options: {
      returnRejectedPromiseOnError: true,
      interceptors: {
        request: [
          ({getState}, config) => {
            return {
              ...config,
              headers: {
                Authorization: config.Authorization ? config.Authorization : '',
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
            };
          },
        ],
        response: [
          {
            success: (store, response) => response,
            error: (store, error) => {
              if (error.response && error.response.data.status === 401) {
                return AsyncStorage.multiRemove([
                  '@Norden:user',
                  '@Norden:token',
                ]);
              }
              console.log('error', error);
              return Promise.reject(error);
            },
          },
        ],
      },
    },
  },
};
