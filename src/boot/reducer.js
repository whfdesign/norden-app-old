import {combineReducers} from 'redux';

import firstAccess from '~/pages/FirstAccess/reducer';
import login from '~/pages/Login/reducers';
import forgotPassword from '~/pages/ForgotPassword/reducers';
import contractSelector from '~/pages/ContractSelector/reducers';
import main from '~/pages/Main/reducers';

export default combineReducers({
  firstAccess,
  login,
  forgotPassword,
  contractSelector,
  main,
});
