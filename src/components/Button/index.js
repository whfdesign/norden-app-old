import React, {Component} from 'react';
import {View, TouchableOpacity, Text} from 'react-native';

import {colors} from '~/assets/styles';
import componentStyles from './styles';

import ArrowRight from '~/assets/svg/arrow-right.svg';

class NordenButton extends Component {
  render() {
    if (this.props.disabled) {
      return (
        <View
          style={{
            ...componentStyles.buttonPrimary,
            ...componentStyles.buttonDisabled,
          }}>
          <Text
            style={{
              ...componentStyles.buttonTextPrimary,
              ...componentStyles.buttonTextDisabled,
            }}>
            {this.props.text}
          </Text>
        </View>
      );
    }

    return (
      <TouchableOpacity
        {...this.props}
        style={{
          ...componentStyles.buttonPrimary,
          ...componentStyles[`button${this.props.class}`],
          ...this.props.style,
        }}>
        <Text
          style={{
            ...componentStyles.buttonTextPrimary,
            ...componentStyles[`buttonText${this.props.class}`],
          }}>
          {this.props.text}
        </Text>
        {this.props.showArrow && (
          <ArrowRight
            style={{...componentStyles.buttonArrowRight}}
          />
        )}
      </TouchableOpacity>
    );
  }
}

export default NordenButton;
