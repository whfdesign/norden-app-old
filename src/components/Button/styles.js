import {colors, typography, shadownAndRadius} from '~/assets/styles/index';

export default {
  buttonPrimary: {
    ...shadownAndRadius.shadow200,
    height: 48,
    backgroundColor: colors.NORDEN_MEDIUM_BLUE,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: colors.NORDEN_BLUE,
    borderRadius: 8,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  buttonAccent: {
    backgroundColor: colors.NORDEN_GOLD,
    borderColor: colors.NORDEN_DARK_GOLD,
  },
  buttonSecondary: {
    backgroundColor: 'transparent',
    shadowColor: 'transparent',
  },
  buttonAccentSecondary: {
    backgroundColor: 'transparent',
    borderColor: colors.NORDEN_DARK_GOLD,
    shadowColor: 'transparent',
  },
  buttonInline: {
    borderWidth: 0,
    backgroundColor: 'transparent',
    shadowColor: 'transparent',
  },
  buttonDisabled: {
    backgroundColor: colors.NORDEN_GRAY,
    borderWidth: 0,
    shadowColor: 'transparent',
  },
  buttonTextPrimary: {
    ...typography.bodyNormalText,
    paddingHorizontal: 23,
    color: colors.NORDEN_WHITE,
    textTransform: 'uppercase',
  },
  buttonTextSecondary: {
    color: colors.NORDEN_MEDIUM_BLUE,
  },
  buttonTextAccentSecondary: {
    color: colors.NORDEN_GOLD,
  },
  buttonTextInline: {
    ...typography.tagSubtitleText,
    color: colors.NORDEN_DARK_BLUE,
  },
  buttonTextDisabled: {
    color: colors.WHITE,
  },
  buttonArrowRight: {
    position: 'absolute',
    right: 27.5,
    color: colors.NORDEN_WHITE
  },
};
