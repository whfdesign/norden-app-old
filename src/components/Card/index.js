import React, {Component, useState} from 'react';
import {View, TouchableOpacity, Text, Image} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import LogoWhite from '~/assets/svg/logo-white.svg';
import Logo from '~/assets/svg/logo.svg';
import Heart from '~/assets/svg/heart.svg';

import componentStyles from './styles';
import {colors, shadownAndRadius} from '~/assets/styles/index';

class NordenCard extends Component {
  state = {
    flipped: false,
  };

  render() {
    return (
      <TouchableOpacity
        style={{
          ...this.props.style,
          ...shadownAndRadius.shadow400,
        }}
        onPress={() => this.setState({flipped: !this.state.flipped})}>
        {!this.state.flipped && (
          <LinearGradient
            style={componentStyles.card}
            colors={[colors.NORDEN_BLUE, colors.NORDEN_LIGHT_BLUE2]}>
            <View style={[componentStyles.cardContainer]}>
              <LogoWhite />
              <Heart style={componentStyles.icon} />
            </View>
            <View>
              <Text style={componentStyles.generalText}>1689437246</Text>
              <Text style={componentStyles.nameText}>
                Silvia Silvana da Silva
              </Text>
              <View style={componentStyles.flexDirectionRow}>
                <Text style={componentStyles.generalText}>12/12/1994</Text>
                <Text style={componentStyles.generalText}>12/12/2019</Text>
              </View>
              <Text style={componentStyles.generalText}>Plano Pleno</Text>
            </View>
          </LinearGradient>
        )}
        {this.state.flipped && (
          <View style={componentStyles.cardFlipped}>
            <View style={{...componentStyles.cardContainer, marginBottom: 20}}>
              <Logo />
              <View style={componentStyles.flexDirectionRow}>
                <Text style={componentStyles.generalBackText}>
                  sac@meunorden.com
                </Text>
                <Text style={componentStyles.generalBackText}>
                  16 3362 2200
                </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginBottom: 20}}>
              <View style={{flex: 1, marginRight: 15}}>
                <Text style={componentStyles.backTitle}>
                  Coletivo Empresarial
                </Text>

                <View style={componentStyles.flexDirectionRow}>
                  <Text style={componentStyles.labelText}>
                    Registro do produto
                  </Text>
                  <Text style={componentStyles.boldBackText}>999.999/99-9</Text>
                </View>
                <View style={componentStyles.flexDirectionRow}>
                  <Text style={componentStyles.labelText}>Abrangência</Text>
                  <Text style={componentStyles.boldBackText}>
                    Grupo de Municípios
                  </Text>
                </View>
                <View style={componentStyles.flexDirectionRow}>
                  <Text style={componentStyles.labelText}>CNS</Text>
                  <Text style={componentStyles.boldBackText}>728634368322</Text>
                </View>
              </View>
              <View style={{flex: 1}}>
                <Text style={componentStyles.backTitle}>Amb.+Hosp. c/ob.</Text>

                <View style={componentStyles.flexDirectionRow}>
                  <Text style={componentStyles.labelText}>Acomodação</Text>
                  <Text style={componentStyles.boldBackText}>
                    Não se aplica
                  </Text>
                </View>
                <View style={componentStyles.flexDirectionRow}>
                  <Text style={componentStyles.labelText}>Contratante</Text>
                  <Text style={componentStyles.boldBackText}>Empresa</Text>
                </View>
                <View style={componentStyles.flexDirectionRow}>
                  <Text style={componentStyles.labelText}>CPT até</Text>
                  <Text style={componentStyles.boldBackText}>00/00/0000</Text>
                </View>
              </View>
            </View>
            <View style={componentStyles.footer}>
              <Image source={require('~/assets/images/ans.png')} />
              <View style={componentStyles.flexDirectionRow}>
                <Text style={componentStyles.generalBackText}>
                  ANS 0800 7019656
                </Text>
                <Text style={componentStyles.generalBackText}>
                  www.ans.gov.br
                </Text>
              </View>
            </View>
          </View>
        )}
      </TouchableOpacity>
    );
  }
}

export default NordenCard;
