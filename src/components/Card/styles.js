import {Dimensions} from 'react-native';
import {colors, typography, shadownAndRadius} from '~/assets/styles/index';

export default {
  card: {
    ...shadownAndRadius.shadow400,
    ...shadownAndRadius.borderRadius12,
    width: Dimensions.get('window').width * 0.9,
    backgroundColor: colors.NORDEN_MEDIUM_BLUE,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: colors.NORDEN_BLUE,
    boxSizing: 'border-box',
    marginBottom: 20,
    padding: 20,
    height: 190,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  cardFlipped: {
    ...shadownAndRadius.borderRadius12,
    backgroundColor: colors.NORDEN_LIGHT_GRAY,
    borderColor: colors.NORDEN_GRAY,
    borderWidth: 1,
    borderStyle: 'solid',
    boxSizing: 'border-box',
    marginBottom: 20,
    padding: 20,
    height: 190,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  cardContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  icon: {
    color: colors.WHITE,
  },
  generalText: {
    ...typography.bodyMicroText,
    color: colors.NORDEN_WHITE,
    marginVertical: 2,
    marginRight: 15,
  },
  nameText: {
    ...typography.labelText,
    color: colors.NORDEN_WHITE,
    marginVertical: 2,
  },
  flexDirectionRow: {
    flexDirection: 'row',
  },
  generalBackText: {
    ...typography.carteirinhaText,
    color: colors.NORDEN_LIGHT_BLUE,
    marginLeft: 15,
  },
  backTitle: {
    ...typography.bodyMicroText,
    fontWeight: '600',
    color: colors.NORDEN_BLUE,
    marginBottom: 4,
  },
  labelText: {
    ...typography.carteirinhaText,
    color: colors.NORDEN_BLUE,
    marginVertical: 2,
    marginRight: 10,
  },
  boldBackText: {
    ...typography.carteirinhaText,
    fontWeight: '600',
    color: colors.NORDEN_BLUE,
    marginVertical: 2,
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
};
