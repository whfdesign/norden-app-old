import React, {Component} from 'react';
import {View, Text, TextInput} from 'react-native';
import CheckBox from '@react-native-community/checkbox';

import componentStyles from './styles';
import {colors} from '~/assets/styles';

class NordenCheckbox extends Component {
  render() {
    return (
      <View style={componentStyles.view}>
        <CheckBox
          {...this.props}
          style={componentStyles.checkbox}
          tintColor={colors.NORDEN_LIGHT_BLUE2}
          onFillColor={colors.NORDEN_LIGHT_BLUE}
          onTintColor={colors.NORDEN_LIGHT_BLUE}
          onCheckColor={colors.NORDEN_WHITE}
          boxType="square"
        />
        <Text
          style={{
            ...componentStyles.label,
          }}>
          {this.props.label}
        </Text>
      </View>
    );
  }
}

export default NordenCheckbox;
