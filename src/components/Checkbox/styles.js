import {colors, shadownAndRadius, typography} from '~/assets/styles/index';

export default {
  view: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 12,
  },
  checkbox: {
    ...shadownAndRadius.shadow100,
    width: 20,
    height: 20,
    marginRight: 8,
  },
  label: {
    flex: 1,
    ...typography.bodySmallText,
    color: colors.NORDEN_LIGHT_BLUE2,
  },
};
