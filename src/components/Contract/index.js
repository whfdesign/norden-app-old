import React, {Component, useState} from 'react';
import {View, TouchableOpacity, Text} from 'react-native';

import ArrowRight from '~/assets/svg/arrow-right.svg';

import componentStyles from './styles';

class NordenContract extends Component {
  render() {
    return (
      <TouchableOpacity
        style={[componentStyles.card]}
        onPress={this.props.onPress}>
        <View style={[componentStyles.cardContainer]}>
          <Text style={[componentStyles.staticInfo]}>
            Plano Integrado Referência
          </Text>
          <Text style={[componentStyles.dynamicInfo]}>{this.props.plan}</Text>
          <Text style={[componentStyles.staticInfo]}>
            Nº do seu Cartão Norden
          </Text>
          <Text style={[componentStyles.dynamicInfo]}>{this.props.card}</Text>
        </View>
        <View style={[componentStyles.button]}>
          <Text style={[componentStyles.text]}>SELECIONAR</Text>
          <ArrowRight style={componentStyles.arrow} />
        </View>
      </TouchableOpacity>
    );
  }
}

export default NordenContract;
