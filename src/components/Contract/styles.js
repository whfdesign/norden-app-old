import {Dimensions} from 'react-native';

import {colors, typography, shadownAndRadius} from '~/assets/styles';
import {color} from "react-native-reanimated";

export default {
  card: {
    ...shadownAndRadius.shadow200,
    ...shadownAndRadius.borderRadius4,
    width: Dimensions.get('window').width * 0.9,
    backgroundColor: colors.NORDEN_LIGHT_BLUE,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: colors.NORDEN_LIGHT_BLUE2,
    boxSizing: 'border-box',
    marginBottom: 20,
  },
  cardContainer: {
    paddingTop: 20,
    paddingHorizontal: 20,
    paddingBottom: 12,
  },
  staticInfo: {
    ...typography.bitTitle,
    color: colors.WHITE,
  },
  dynamicInfo: {
    ...typography.bodySmallText,
    color: colors.NORDEN_GRAY,
    marginBottom: 8,
  },
  button: {
    paddingVertical: 12,
    paddingHorizontal: 20,
    borderTopWidth: 1,
    borderTopStyle: 'solid',
    borderColor: colors.NORDEN_LIGHT_BLUE2,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  text: {
    ...typography.tagSubtitleText,
    color: colors.NORDEN_WHITE,
  },
  arrow: {
    color: colors.NORDEN_WHITE,
  },
};
