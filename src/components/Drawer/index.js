import React from 'react';
import {
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import {View, Text, TouchableOpacity, Image} from 'react-native';

import componentStyles from './styles';

import User from '~/assets/svg/user.svg';
import File from '~/assets/svg/file.svg';
import Info from '~/assets/svg/info.svg';
import Support from '~/assets/svg/support.svg';
import Close from '~/assets/svg/close.svg';

const NordenDrawer = (props) => (
  <DrawerContentScrollView
    {...props}
    contentContainerStyle={componentStyles.drawerContent}>
    <View style={componentStyles.header}>
      <Text style={componentStyles.title}>Menu</Text>
      <TouchableOpacity onPress={() => props.navigation.closeDrawer()}>
        <Close style={componentStyles.iconTitle} />
      </TouchableOpacity>
    </View>

    <View style={componentStyles.menuContent}>
      <TouchableOpacity
        style={componentStyles.menuGroup}
        onPress={() => props.navigation.navigate('Auth', {screen: 'Profile'})}>
        <User style={componentStyles.menuIcon} />
        <Text style={componentStyles.menuText}>Meu Perfil</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={componentStyles.menuGroup}
        onPress={() =>
          props.navigation.navigate('Auth', {screen: 'ShortageHistory'})
        }>
        <File style={componentStyles.menuIcon} />
        <Text style={componentStyles.menuText}>Minhas Carências</Text>
      </TouchableOpacity>
      <TouchableOpacity style={componentStyles.menuGroup}>
        <Info style={componentStyles.menuIcon} />
        <Text style={componentStyles.menuText}>Acessar ANS</Text>
      </TouchableOpacity>
      <TouchableOpacity style={componentStyles.menuGroup}>
        <Support style={componentStyles.menuIcon} />
        <Text style={componentStyles.menuText}>Pedir Ajuda</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={componentStyles.menuGroup}
        onPress={() =>
          props.navigation.navigate('Auth', {screen: 'PrivacyPolicy'})
        }>
        <Info style={componentStyles.menuIcon} />
        <Text style={componentStyles.menuText}>
          Ver Política de Privacidade
        </Text>
      </TouchableOpacity>
    </View>

    <View style={componentStyles.footer}>
      <Image source={require('~/assets/images/ans.png')} />
      <Text style={componentStyles.footerText}>versão 0.1.1</Text>
    </View>
  </DrawerContentScrollView>
);

export default NordenDrawer;
