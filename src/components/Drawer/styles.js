import {colors, shadownAndRadius, typography} from '~/assets/styles/index';

export default {
  drawerContent: {
    ...shadownAndRadius.shadow400,
    backgroundColor: colors.NORDEN_BLUE,
    borderLeftWidth: 1,
    borderLeftStyle: 'solid',
    borderLeftColor: colors.NORDEN_LIGHT_BLUE2,
    flex: 1,
    alignItems: 'stretch',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 27,
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomStyle: 'solid',
    borderBottomColor: colors.NORDEN_LIGHT_BLUE,
    justifyContent: 'space-between',
  },
  title: {
    ...typography.smallTitle,
    color: colors.NORDEN_WHITE,
  },
  iconTitle: {
    color: colors.NORDEN_WHITE,
  },
  menuContent: {
    flex: 1,
    paddingVertical: 40,
    paddingHorizontal: 25,
  },
  menuGroup: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  menuIcon: {
    color: colors.NORDEN_LIGHT_BLUE,
    marginRight: 10,
  },
  menuText: {
    ...typography.bodySmallText,
    color: colors.NORDEN_WHITE,
  },
  footer: {
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  footerText: {
    ...typography.bodyMicroText,
    color: colors.NORDEN_LIGHT_BLUE2,
  },
};
