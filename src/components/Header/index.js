import React from 'react';
import {View, Image, Text, TouchableOpacity} from 'react-native';

import componentStyles from './styles';

import ArrowLeft from '~/assets/svg/arrow-left.svg';
import Menu from '~/assets/svg/menu.svg';

const NordenHeader = (props) => (
  <View style={componentStyles.header}>
    <View style={componentStyles.headerContainer}>
      {props.title ? (
        <>
          <TouchableOpacity onPress={() => props.navigation.goBack()}>
            <ArrowLeft style={componentStyles.menu} />
          </TouchableOpacity>
          <Text style={componentStyles.title}>{props.title}</Text>
        </>
      ) : (
        <>
          <View style={componentStyles.image}>
            <Image source={require('~/assets/images/user.png')} />
          </View>
          <View>
            <Text style={componentStyles.name}>Marlon Trovão</Text>
            <Text style={componentStyles.plan}>Plano integrado Referência</Text>
          </View>
        </>
      )}
    </View>
    <TouchableOpacity onPress={() => props.navigation.toggleDrawer()}>
      <Menu style={componentStyles.menu} />
    </TouchableOpacity>
  </View>
);

export default NordenHeader;
