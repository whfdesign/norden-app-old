import {colors, typography} from '~/assets/styles/index';

export default {
  header: {
    backgroundColor: colors.NORDEN_MEDIUM_BLUE,
    padding: 20,
    borderBottomWidth: 1,
    borderBottomStyle: 'solid',
    borderBottomColor: colors.NORDEN_LIGHT_BLUE,
    flexDirection: 'row',
    alignItems: 'center',
    height: 85,
  },
  headerContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    ...typography.smallTitle,
    color: colors.NORDEN_WHITE,
    marginLeft: 15,
  },
  image: {
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: colors.NORDEN_MEDIUM_GRAY,
    borderRadius: 44,
    marginRight: 12,
  },
  name: {
    ...typography.bodyNormalText,
    color: colors.WHITE,
  },
  plan: {
    ...typography.bodySmallText,
    color: colors.NORDEN_GRAY,
  },
  menu: {
    color: colors.WHITE,
  },
};
