import React, {Component} from 'react';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import {TextInputMask} from 'react-native-masked-text';

import componentStyles from './styles';

import Eye from '~/assets/svg/eye.svg';
import EyeSlash from '~/assets/svg/eye-slash.svg';

class NordenInput extends Component {
  state = {
    secureTextEntry: this.props.secureTextEntry,
  };

  getMaskType() {
    if (this.props.maskType == 'document') {
      let valueUnmasked = this.props.value.replace(/\D/g, '');

      if (valueUnmasked.length <= 11) {
        return '999.999.999-999';
      } else if (valueUnmasked.length >= 12) {
        return '99.999.999/9999-99';
      }
    }
  }

  toggleSecureTextEntry = () => {
    this.setState({secureTextEntry: !this.state.secureTextEntry});
  };

  render() {
    return this.props.masked ? (
      <TextInputMask
        type={'custom'}
        options={{
          mask: this.getMaskType(),
        }}
        {...this.props}
        style={{
          ...componentStyles.input,
          ...componentStyles[`input${this.props.class}`],
          ...this.props.style,
        }}
      />
    ) : (
      <View style={componentStyles.inputContainer}>
        <TextInput
          {...this.props}
          secureTextEntry={this.state.secureTextEntry}
          style={{
            ...componentStyles.input,
            ...componentStyles[`input${this.props.class}`],
            ...this.props.style,
          }}
        />
        {this.state.secureTextEntry === true ? (
          <TouchableOpacity
            style={{
              ...componentStyles.iconButton,
            }}
            onPress={this.toggleSecureTextEntry}>
            <Eye
              style={{
                ...componentStyles.icon,
              }}
            />
          </TouchableOpacity>
        ) : this.state.secureTextEntry === false ? (
          <TouchableOpacity
            style={{
              ...componentStyles.iconButton,
            }}
            onPress={this.toggleSecureTextEntry}>
            <EyeSlash
              style={{
                ...componentStyles.icon,
              }}
            />
          </TouchableOpacity>
        ) : null}
      </View>
    );
  }
}

export default NordenInput;
