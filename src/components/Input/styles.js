import {colors, typography, shadownAndRadius} from '~/assets/styles/index';

export default {
  inputContainer: {
    position: 'relative',
  },
  input: {
    ...shadownAndRadius.borderRadius4,
    ...shadownAndRadius.shadow100,
    backgroundColor: colors.WHITE,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: colors.NORDEN_LIGHT_BLUE,
    paddingHorizontal: 12,
    height: 44,
    marginBottom: 4,
  },
  inputFocused: {
    borderColor: colors.NORDEN_LIGHT_BLUE,
  },
  inputDisabled: {
    backgroundColor: colors.NORDEN_WHITE,
  },
  inputError: {
    borderColor: colors.NORDEN_ERROR,
    color: colors.NORDEN_ERROR,
  },
  iconButton: {
    position: 'absolute',
    top: 12,
    right: 13,
  },
  icon: {
    color: colors.NORDEN_LIGHT_BLUE,
  },
};
