import React, {Component} from 'react';
import {Text} from 'react-native';

import componentStyles from './styles';

class NordenLabel extends Component {
  render() {
    return (
      <Text
        {...this.props}
        style={{
          ...componentStyles.label,
          ...this.props.style,
          ...componentStyles[`label${this.props.class}`],
        }}>
        {this.props.text}
      </Text>
    );
  }
}

export default NordenLabel;
