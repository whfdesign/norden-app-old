import {colors, typography} from '~/assets/styles/index';

export default {
  label: {
    ...typography.labelText,
    color: colors.NORDEN_BLACK,
    marginBottom: 8,
  },
  labelError: {
    color: colors.NORDEN_ERROR,
  },
  labelTagError: {
    ...typography.bodySmallText,
    backgroundColor: colors.NORDEN_ERROR,
    borderRadius: 4,
    paddingVertical: 5,
    paddingHorizontal: 12,
    color: colors.WHITE,
    textTransform: 'none',
    overflow: 'hidden',
  },
};
