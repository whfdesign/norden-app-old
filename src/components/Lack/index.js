import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {List, ListItem} from 'native-base';
import Svg, {Circle, Line} from 'react-native-svg';

import {colors} from '~/assets/styles';
import componentStyles from './styles';

import NoLack from '~/assets/svg/no-lack.svg';
import NordenButton from '~/components/Button';

const NordenLack = (props) => (
  <View style={componentStyles.lackContainer}>
    {props.lacks.length > 0 && (
      <>
        <Text style={componentStyles.title}>Carências</Text>
        <Text style={componentStyles.start}>Plano ativado em: 01/09/2020.</Text>

        <List>
          {props.lacks.map((lack, index) => (
            <ListItem style={componentStyles.listItem} key={index}>
              <View style={componentStyles.listItemTimeline}>
                <Svg height="20" width="20">
                  <Circle
                    cx="9"
                    cy="9"
                    r="9"
                    fill={colors.NORDEN_MEDIUM_GRAY}
                  />
                </Svg>
              </View>
              <View style={componentStyles.listItemView}>
                <Text style={componentStyles.listItemText}>{lack.name}</Text>
                <View style={componentStyles.listItemBudget}>
                  <Text style={componentStyles.listItemText}>
                    {lack.days} dias
                  </Text>
                </View>
              </View>
            </ListItem>
          ))}
        </List>
        <View style={componentStyles.buttonContainer}>
          <NordenButton
            onPress={() =>
              props.navigation.navigate('Auth', {screen: 'ShortageHistory'})
            }
            style={componentStyles.button}
            text="+ Carências"
            class="AccentSecondary"
          />
        </View>
      </>
    )}
    {props.lacks.length == 0 && (
      <>
        <NoLack />
        <View style={componentStyles.buttonContainer}>
          <NordenButton
            onPress={() =>
              props.navigation.navigate('Auth', {screen: 'ShortageHistory'})
            }
            style={componentStyles.button}
            text="Histórico de Carências"
            class="AccentSecondary"
          />
        </View>
      </>
    )}
  </View>
);

export default NordenLack;
