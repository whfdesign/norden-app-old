import {colors, typography} from '~/assets/styles/index';

export default {
  lackContainer: {
    width: '100%',
    paddingHorizontal: 20,
  },
  title: {
    ...typography.tagSubtitleText,
    color: colors.NORDEN_BLACK,
    textTransform: 'uppercase',
  },
  start: {
    ...typography.bodyMicroText,
    color: colors.NORDEN_LIGHT_BLUE,
    marginBottom: 15,
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    marginTop: 20,
  },
  listItem: {
    borderBottomWidth: 0,
    paddingVertical: 6,
    flexDirection: 'row',
    alignItems: 'center',
  },
  listItemTimeline: {
    paddingBottom: 6,
    paddingLeft: 8,
    paddingRight: 24,
  },
  listItemView: {
    flex: 1,
    borderBottomWidth: 1,
    borderBottomStyle: 'solid',
    borderBottomColor: colors.NORDEN_LIGHT_GRAY,
    paddingBottom: 12,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  listItemText: {
    ...typography.bodySmallText,
    color: colors.NORDEN_BLUE,
  },
  listItemBudget: {
    backgroundColor: colors.NORDEN_MEDIUM_GRAY,
    borderRadius: 12,
    paddingVertical: 5,
    paddingHorizontal: 8,
    marginRight: 2,
  },
};
