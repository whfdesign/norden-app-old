import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Animated,
  Easing,
  Text,
  ViewPropTypes,
  View,
} from 'react-native';

import componentStyles from './styles';

import CheckCircle from '~/assets/svg/check-circle.svg';
import WarningOutline from '~/assets/svg/warning-outline.svg';

const notExist = -1;

export const Type = {
  success: 'success',
  error: 'error',
};
export const Position = {
  top: 'top',
  center: 'center',
  bottom: 'bottom',
};
export const Duration = {
  short: 1500,
  long: 2500,
  infinite: 0,
};

export default class Toast extends Component {
  static Type = Type;
  static Position = Position;
  static Duration = Duration;

  static propTypes = {
    type: PropTypes.oneOf([Type.success, Type.error]),
    position: PropTypes.oneOf([Position.top, Position.center, Position.bottom]),
    style: ViewPropTypes.style,
    textStyle: Text.propTypes.style,
    fadeInDuration: PropTypes.number,
    fadeOutDuration: PropTypes.number,
    duration: PropTypes.number,
    opacity: PropTypes.number,
    positionValue: PropTypes.number,
  };

  static defaultProps = {
    style: {},
    type: Type.error,
    position: Position.top,
    textStyle: {},
    fadeInDuration: 300,
    fadeOutDuration: 300,
    duration: Duration.long,
    opacity: 1,
    positionValue: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      message: null,
    };
    this.opacity = new Animated.Value(0);
    this._initVariate();
  }

  componentWillUnmount() {
    this.timer && clearTimeout(this.timer);
  }

  _initVariate = () => {
    this.type = 'success';
    this.position = notExist;
    this.duration = notExist;
    this.didShow = false;
    this.timer && clearTimeout(this.timer);
  };

  _startAnimation = () => {
    this.didShow = true;
    const {duration, opacity, fadeInDuration} = this.props;
    let tmpDuration = duration;
    if (this.duration !== notExist) {
      tmpDuration = this.duration;
    }
    this.opacity.setValue(0);
    Animated.timing(this.opacity, {
      toValue: opacity,
      duration: fadeInDuration,
      useNativeDriver: true,
      easing: Easing.out(Easing.linear),
    }).start(() => {
      if (tmpDuration !== Duration.infinite) {
        this.timer = setTimeout(() => {
          this.close();
        }, tmpDuration);
      }
    });
  };

  show(
    message = null,
    type = Type.success,
    duration = Duration.long,
    position = Position.bottom,
  ) {
    if (this.didShow) {
      this.close(true);
    }
    this.type = type;
    this.duration = duration;
    this.position = position;
    this.setState((state) => {
      state.message = message;
      return state;
    });
  }

  close(isNow = false) {
    if (isNow) {
      this.setState((state) => {
        state.message = null;
        return state;
      });
      this._initVariate();
    } else {
      const {opacity, fadeOutDuration} = this.props;
      this.opacity.setValue(opacity);
      Animated.timing(this.opacity, {
        toValue: 0,
        duration: fadeOutDuration,
        useNativeDriver: true,
        easing: Easing.out(Easing.linear),
      }).start(() => {
        this.show();
        this._initVariate();
      });
    }
  }

  render() {
    const {style, textStyle, positionValue, position} = this.props;
    const {message} = this.state;
    const visible = message != void 0 && message.length > 0;
    let positionType = position;
    if (this.position !== notExist) {
      positionType = this.position;
    }
    visible && this._startAnimation();
    let toastStyle = {};
    return visible ? (
      <View
        style={[componentStyles.toast, {...toastStyle}]}
        pointerEvents="box-none">
        <Animated.View
          style={[
            componentStyles.textView,
            {...style, opacity: this.opacity},
            this.type == 'success' && componentStyles.textViewSuccess,
            this.type == 'error' && componentStyles.textViewError,
          ]}>
          <View
            style={[
              this.type == 'success' && componentStyles.successIconContainer,
              this.type == 'error' && componentStyles.errorIconContainer,
            ]}>
            {this.type == 'success' && <CheckCircle style={componentStyles.icon} />}
            {this.type == 'error' && <WarningOutline style={componentStyles.icon} />}
          </View>

          <Text style={[componentStyles.text, {...textStyle}]}>{message}</Text>
        </Animated.View>
      </View>
    ) : null;
  }
}
