import {colors, typography} from '~/assets/styles/index';

export default {
  toast: {
    elevation: 1000,
    zIndex: 10000,
    left: 10,
    right: 10,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    top: 68,
  },
  textView: {
    width: '90%',
    borderRadius: 5,
    borderWidth: 1,
    borderStyle: 'solid',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  textViewSuccess: {
    backgroundColor: colors.NORDEN_SUCCESS,
    borderColor: colors.NORDEN_DARK_SUCCESS,
  },
  textViewError: {
    backgroundColor: colors.NORDEN_ERROR,
    borderColor: colors.NORDEN_DARK_ERROR,
  },
  successIconContainer: {
    backgroundColor: colors.NORDEN_DARK_SUCCESS,
    padding: 12,
    marginRight: 8,
  },
  errorIconContainer: {
    backgroundColor: colors.NORDEN_DARK_ERROR,
    padding: 12,
    marginRight: 8,
  },
  icon: {
    color: colors.NORDEN_WHITE,
  },
  dangerIcon: {
    backgroundColor: colors.NORDEN_DARK_ERROR,
    padding: 12,
    color: colors.NORDEN_WHITE,
  },
  text: {
    ...typography.bodySmallText,
    color: colors.WHITE,
  },
};
