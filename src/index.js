import 'react-native-gesture-handler';

import React, {Component} from 'react';
import {SafeAreaView} from 'react-native';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';

import * as Navigator from '~/services/navigator';
import * as Toast from '~/services/toast';

import colors from '~/assets/styles/colors';

import NordenToast from '~/components/Toast';
import NordenDrawer from '~/components/Drawer';

import FirstAccessContainer from '~/pages/FirstAccess/container';
import Login from '~/pages/Login';
import ForgotPassword from '~/pages/ForgotPassword';
import ContractSelector from '~/pages/ContractSelector';

import Main from '~/pages/Main';
import Profile from '~/pages/Profile';
import ChangePassword from '~/pages/ChangePassword';
import Address from '~/pages/Address';
import AddressForm from '~/pages/AddressForm';
import Data from '~/pages/Data';
import DataForm from '~/pages/DataForm';
import PrivacyPolicy from '~/pages/PrivacyPolicy';
import ShortageHistory from '~/pages/ShortageHistory';

import Logo from '~/assets/svg/logo.svg';

import {store} from './boot/store';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

function NonAuth() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="FirstAccess" component={FirstAccessContainer} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
      <Stack.Screen name="ContractSelector" component={ContractSelector} />
    </Stack.Navigator>
  );
}

function Auth() {
  return (
    <Drawer.Navigator
      drawerPosition="right"
      drawerContent={(props) => <NordenDrawer {...props} />}>
      <Drawer.Screen name="Main" component={Main} />
      <Drawer.Screen name="Profile" component={Profile} />
      <Drawer.Screen name="ChangePassword" component={ChangePassword} />
      <Drawer.Screen name="Address" component={Address} />
      <Drawer.Screen name="AddressForm" component={AddressForm} />
      <Drawer.Screen name="Data" component={Data} />
      <Drawer.Screen name="DataForm" component={DataForm} />
      <Drawer.Screen name="PrivacyPolicy" component={PrivacyPolicy} />
      <Drawer.Screen name="ShortageHistory" component={ShortageHistory} />
    </Drawer.Navigator>
  );
}

class App extends Component {
  state = {
    loading: true,
  };

  render() {
    return (
      <Provider store={store}>
        <SafeAreaView style={{flex: 1}}>
          <NordenToast ref={Toast.setContainer} />

          <NavigationContainer ref={Navigator.setContainer}>
            <Stack.Navigator
              screenOptions={{
                headerStyle: {
                  backgroundColor: colors.NORDEN_MEDIUM_GRAY,
                  shadowColor: 'transparent',
                  elevation: 0,
                  shadowOpacity: 0,
                },
                headerTitleAlign: 'center',
                headerTitle: (props) => <Logo width={120} height={120} />,
              }}>
              <Stack.Screen name="NonAuth" component={NonAuth} />
              <Stack.Screen
                name="Auth"
                component={Auth}
                options={{headerShown: false}}
              />
            </Stack.Navigator>
          </NavigationContainer>
        </SafeAreaView>
      </Provider>
    );
  }
}

export default App;
