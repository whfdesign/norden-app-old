import React, {Component} from 'react';
import {View, SafeAreaView, ScrollView, Text, TouchableOpacity} from 'react-native';
import {Container} from 'native-base';

import screenStyles from './styles';

import ArrowRight from '~/assets/svg/arrow-right.svg';

import NordenHeader from '~/components/Header';

const Address = ({navigation}) => (
  <SafeAreaView style={{flex: 1}}>
    <Container
      scrollEnabled={false}
      style={screenStyles.content}
      contentContainerStyle={{flex: 1}}>
      <NordenHeader title="Meu Endereço" navigation={navigation} />
      <ScrollView contentContainerStyle={screenStyles.paddingArea}>
        <Text style={screenStyles.title}>Endereço cadastrado</Text>

        <TouchableOpacity
          style={screenStyles.addressCard}
          onPress={() => navigation.navigate('Auth', {screen: 'AddressForm'})}>
          <View style={screenStyles.addressCardContent}>
            <Text style={screenStyles.addressCardContentLabel}>Endereço</Text>
            <Text
              style={{...screenStyles.addressCardContentValue, marginBottom: 12}}>
              Irmao basilio zamodinsk, 2080 Franca - SP
            </Text>

            <Text style={screenStyles.addressCardContentLabel}>CEP</Text>
            <Text style={screenStyles.addressCardContentValue}>14.409-117</Text>
          </View>
          <View style={screenStyles.addressCardFooter}>
            <Text style={screenStyles.addressCardFooterText}>Alterar</Text>
            <ArrowRight style={screenStyles.addressCardFooterIcon} />
          </View>
        </TouchableOpacity>
      </ScrollView>
    </Container>
  </SafeAreaView>
);

export default Address;
