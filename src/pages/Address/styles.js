import {colors, shadownAndRadius, typography} from '~/assets/styles/index';

export default {
  content: {
    backgroundColor: colors.NORDEN_BLUE,
    flexDirection: 'column',
  },
  mainContent: {
    width: '100%',
  },
  paddingArea: {
    flex: 1,
    padding: 40,
    alignItems: 'stretch',
  },
  title: {
    ...typography.tinyTitle,
    color: colors.NORDEN_WHITE,
    marginBottom: 8,
  },
  addressCard: {
    ...shadownAndRadius.shadow300,
    backgroundColor: colors.NORDEN_LIGHT_BLUE,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: colors.NORDEN_LIGHT_BLUE2,
    borderRadius: 4,
  },
  addressCardContent: {
    padding: 20,
  },
  addressCardContentLabel: {
    ...typography.bodySmallText,
    color: colors.NORDEN_GRAY,
  },
  addressCardContentValue: {
    ...typography.bitTitle,
    color: colors.WHITE,
  },
  addressCardFooter: {
    paddingVertical: 12,
    paddingHorizontal: 20,
    borderTopWidth: 1,
    borderTopStyle: 'solid',
    borderTopColor: colors.NORDEN_LIGHT_BLUE2,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  addressCardFooterText: {
    ...typography.tagSubtitleText,
    color: colors.NORDEN_WHITE,
  },
  addressCardFooterIcon: {
    color: colors.NORDEN_WHITE,
  },
};
