import React, {Component} from 'react';
import {
  View,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
} from 'react-native';
import {Container} from 'native-base';
import {Formik} from 'formik';
import ImagePicker from 'react-native-image-picker';

import screenStyles from './styles';

import Paperclip from '~/assets/svg/paperclip.svg';

import NordenHeader from '~/components/Header';
import NordenLabel from '~/components/Label';
import NordenInput from '~/components/Input';
import NordenButton from '~/components/Button';
import NordenToast from '~/components/Toast';

function attachImage() {
  const options = {
    title: 'Selecionar comprovante',
    storageOptions: {
      path: 'images',
    },
  };

  ImagePicker.showImagePicker(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    } else {
      const source = {uri: response.uri};

      // You can also display the image using data:
      // const source = { uri: 'data:image/jpeg;base64,' + response.data };

    }
  });
}

const AddressForm = ({navigation}) => (
  <SafeAreaView style={{flex: 1}}>
    <Container
      scrollEnabled={false}
      style={screenStyles.content}
      contentContainerStyle={{flex: 1}}>
      <NordenHeader title="Meu Endereço" navigation={navigation} />
      <NordenToast style={screenStyles.toast} ref={(c) => (this.toast = c)} />
      <ScrollView contentContainerStyle={screenStyles.paddingArea}>
        <Text style={screenStyles.title}>Atualizar endereço</Text>
        <Text style={screenStyles.text}>
          Digite seu novo endereço e anexe um novo comprovante de residência
        </Text>
        <Formik
          initialValues={{
            address: '',
            file: '',
          }}
          onSubmit={(values, {resetForm}) => {
            resetForm({
              address: '',
              file: '',
            });
          }}>
          {({values, handleChange, handleSubmit, errors}) => (
            <View style={screenStyles.formContainer}>
              <View>
                <NordenLabel
                  text="Endereço"
                  style={screenStyles.label}
                  class={errors.address && 'Error'}
                />
                <NordenInput
                  placeholder="Logradouro, nº e complemento"
                  value={values.address}
                  onChangeText={handleChange('address')}
                  autoCompleteType="off"
                  autoCorrect={false}
                  class={errors.address && 'Error'}
                />
                {errors.address && (
                  <NordenLabel class="TagError" text={errors.address} />
                )}

                <TouchableOpacity
                  style={screenStyles.item}
                  onPress={attachImage}>
                  <Paperclip style={screenStyles.itemIcon} />
                  <Text style={screenStyles.itemText}>
                    Anexar comprovante de residência
                  </Text>
                </TouchableOpacity>
                <Text style={screenStyles.helpText}>
                  Apenas arquivos JPG, PNG e PDF.
                </Text>
                <Text style={screenStyles.helpText}>Tamanho máximo 20MB.</Text>
              </View>

              <NordenButton
                text="Confirmar"
                class="Accent"
                onPress={handleSubmit}
                style={{marginTop: 30}}
              />
            </View>
          )}
        </Formik>
      </ScrollView>
    </Container>
  </SafeAreaView>
);

export default AddressForm;
