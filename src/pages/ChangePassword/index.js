import React, {Component} from 'react';
import {
  View,
  SafeAreaView,
  ScrollView,
  Text,
} from 'react-native';
import {Container} from 'native-base';
import {Formik} from 'formik';
import * as Yup from 'yup';

import screenStyles from './styles';

import NordenHeader from '~/components/Header';
import NordenLabel from '~/components/Label';
import NordenInput from '~/components/Input';
import NordenButton from '~/components/Button';
import NordenToast from '~/components/Toast';

const formValidation = Yup.object().shape({
  actualPassword: Yup.string()
    .required('Digite sua senha atual')
    .min(8, 'Ela deve conter ao menos 8 caracteres'),
  newPassword: Yup.string()
    .required('Digite sua nova senha')
    .min(8, 'Use ao menos 8 caracteres'),
});

const ChangePassword = ({navigation}) => (
  <SafeAreaView style={{flex: 1}}>
    <Container
      scrollEnabled={false}
      style={screenStyles.content}
      contentContainerStyle={{flex: 1}}>
      <NordenHeader title="Mudar Senha" navigation={navigation} />
      <NordenToast style={screenStyles.toast} ref={(c) => (this.toast = c)} />
      <ScrollView contentContainerStyle={screenStyles.paddingArea}>
        <Text style={screenStyles.text}>
          Informe sua senha de acesso atual e depois cadastre uma nova senha
        </Text>
        <Formik
          initialValues={{
            actualPassword: '',
            newPassword: '',
          }}
          onSubmit={(values, {resetForm}) => {
            resetForm({
              actualPassword: '',
              newPassword: '',
            });

            this.toast.show('Pronto, sua senha foi alterada', 'success');
          }}
          validationSchema={formValidation}>
          {({values, handleChange, handleSubmit, errors}) => (
            <View style={screenStyles.formContainer}>
              <View>
                <NordenLabel
                  text="Senha atual"
                  style={screenStyles.label}
                  class={errors.actualPassword && 'Error'}
                />
                <NordenInput
                  secureTextEntry={true}
                  placeholder="Mínimo de 8 caracteres"
                  value={values.actualPassword}
                  onChangeText={handleChange('actualPassword')}
                  autoCompleteType="off"
                  autoCorrect={false}
                  class={errors.actualPassword && 'Error'}
                />
                {errors.actualPassword && (
                  <NordenLabel class="TagError" text={errors.actualPassword} />
                )}

                <NordenLabel
                  text="Nova Senha"
                  style={screenStyles.label}
                  class={errors.newPassword && 'Error'}
                />
                <NordenInput
                  secureTextEntry={true}
                  placeholder="Mínimo de 8 caracteres"
                  value={values.newPassword}
                  onChangeText={handleChange('newPassword')}
                  autoCompleteType="off"
                  autoCorrect={false}
                  class={errors.newPassword && 'Error'}
                />
                {errors.newPassword && (
                  <NordenLabel class="TagError" text={errors.newPassword} />
                )}
              </View>

              <NordenButton
                text="Confirmar"
                class="Accent"
                onPress={handleSubmit}
                style={{marginTop: 30}}
              />
            </View>
          )}
        </Formik>
      </ScrollView>
    </Container>
  </SafeAreaView>
);

export default ChangePassword;
