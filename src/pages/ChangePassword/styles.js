import {colors, typography} from '~/assets/styles/index';

export default {
  content: {
    backgroundColor: colors.NORDEN_BLUE,
    flexDirection: 'column',
  },
  mainContent: {
    width: '100%',
  },
  paddingArea: {
    flex: 1,
    padding: 40,
    alignItems: 'stretch',
  },
  text: {
    ...typography.bodySmallText,
    color: colors.NORDEN_WHITE,
    marginBottom: 20,
  },
  label: {
    color: colors.NORDEN_WHITE,
    marginTop: 20,
  },
  toast: {
    marginTop: 68,
  },
  formContainer: {
    flex: 1,
    justifyContent: 'space-between',
  },
};
