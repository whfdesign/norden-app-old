import React, {Component} from 'react';
import {View, SafeAreaView, ScrollView, Text} from 'react-native';
import {Container} from 'native-base';
import {Formik} from 'formik';

import screenStyles from './styles';

import NordenHeader from '~/components/Header';
import NordenLabel from '~/components/Label';
import NordenInput from '~/components/Input';
import NordenButton from '~/components/Button';
import NordenToast from '~/components/Toast';

const DataForm = ({navigation}) => (
  <SafeAreaView style={{flex: 1}}>
    <Container style={screenStyles.content} contentContainerStyle={{flex: 1}}>
      <NordenHeader title="Alterando Contatos" navigation={navigation} />
      <NordenToast style={screenStyles.toast} ref={(c) => (this.toast = c)} />
      <ScrollView contentContainerStyle={screenStyles.paddingArea}>
        <Text style={screenStyles.title}>Meus Dados</Text>
        <Formik
          initialValues={{
            telephone: '',
            cellphone: '',
          }}
          onSubmit={(values, {resetForm}) => {
            resetForm({
              telephone: '',
              cellphone: '',
            });
          }}>
          {({values, handleChange, handleSubmit, errors}) => (
            <View style={screenStyles.formContainer}>
              <View>
                <NordenLabel
                  text="Telefone"
                  style={screenStyles.label}
                  class={errors.telephone && 'Error'}
                />
                <NordenInput
                  placeholder="Telefone"
                  value={values.telephone}
                  onChangeText={handleChange('telephone')}
                  autoCompleteType="off"
                  autoCorrect={false}
                  class={errors.telephone && 'Error'}
                />
                {errors.telephone && (
                  <NordenLabel class="TagError" text={errors.telephone} />
                )}

                <NordenLabel
                  text="Celular"
                  style={screenStyles.label}
                  class={errors.cellphone && 'Error'}
                />
                <NordenInput
                  placeholder="Celular"
                  value={values.cellphone}
                  onChangeText={handleChange('cellphone')}
                  autoCompleteType="off"
                  autoCorrect={false}
                  class={errors.cellphone && 'Error'}
                />
                {errors.cellphone && (
                  <NordenLabel class="TagError" text={errors.cellphone} />
                )}
              </View>

              <NordenButton
                text="Confirmar"
                class="Accent"
                onPress={handleSubmit}
                style={{marginTop: 30}}
              />
            </View>
          )}
        </Formik>
      </ScrollView>
    </Container>
  </SafeAreaView>
);

export default DataForm;
