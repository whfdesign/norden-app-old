import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {SafeAreaView, ScrollView, Text} from 'react-native';
import {Container} from 'native-base';

import screenStyles from './styles';

import NordenContract from '~/components/Contract';

import * as ContractSelectorActions from './actions/index';

const ContractSelector = ({navigation, contracts}) => (
  <SafeAreaView style={{flex: 1}}>
    <Container
      style={screenStyles.mainContent}
      contentContainerStyle={{flex: 1}}
      resetScrollToCoords={{x: 0, y: 0}}>
      <Text style={screenStyles.text}>Pronto!</Text>
      <Text style={screenStyles.text}>Agora selecione um contrato</Text>

      <ScrollView
        style={{flex: 1, marginTop: 20}}
        contentContainerStyle={{alignItems: 'center'}}>
        <NordenContract
          plan="Plano padrão"
          card="0000-00000-00"
          onPress={() => navigation.navigate('Auth', {screen: 'Main'})}
        />
        <NordenContract
          plan="Plano padrão"
          card="0000-00000-00"
          onPress={() => navigation.navigate('Auth', {screen: 'Main'})}
        />
        <NordenContract
          plan="Plano padrão"
          card="0000-00000-00"
          onPress={() => navigation.navigate('Auth', {screen: 'Main'})}
        />
        <NordenContract
          plan="Plano padrão"
          card="0000-00000-00"
          onPress={() => navigation.navigate('Auth', {screen: 'Main'})}
        />
      </ScrollView>
    </Container>
  </SafeAreaView>
);

const mapStateToProps = (state) => ({
  contracts: state.contractSelector.contracts,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ContractSelectorActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ContractSelector);
