import React, {Component} from 'react';
import {
  View,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
} from 'react-native';
import {Container} from 'native-base';

import screenStyles from './styles';

import ArrowRight from '~/assets/svg/arrow-right.svg';

import NordenHeader from '~/components/Header';

const Main = ({navigation}) => (
  <SafeAreaView style={{flex: 1}}>
    <Container
      scrollEnabled={false}
      style={screenStyles.content}
      contentContainerStyle={{flex: 1}}>
      <NordenHeader title="Meus Dados" navigation={navigation} />
      <ScrollView contentContainerStyle={screenStyles.paddingArea}>
        <TouchableOpacity
          style={screenStyles.dataCard}
          onPress={() => navigation.navigate('Auth', {screen: 'DataForm'})}>
          <View style={screenStyles.dataCardContent}>
            <Text style={screenStyles.dataCardContentLabel}>Plano</Text>
            <Text
              style={{...screenStyles.dataCardContentValue, marginBottom: 12}}>
              Pleno Super Nordem
            </Text>

            <Text style={screenStyles.dataCardContentLabel}>Beneficiário</Text>
            <Text
              style={{...screenStyles.dataCardContentValue, marginBottom: 12}}>
              Marlinho Júnior
            </Text>

            <Text style={screenStyles.dataCardContentLabel}>
              Responsável pelo Contrato
            </Text>
            <Text style={screenStyles.dataCardContentValue}>
              Marlon Trovão Senior
            </Text>
          </View>
          <View style={screenStyles.dataCardContent}>
            <Text style={screenStyles.dataCardContentLabel}>Nascimento</Text>
            <Text
              style={{...screenStyles.dataCardContentValue, marginBottom: 12}}>
              18/02/1994
            </Text>

            <Text style={screenStyles.dataCardContentLabel}>
              Nº Beneficiário Norden
            </Text>
            <Text
              style={{...screenStyles.dataCardContentValue, marginBottom: 12}}>
              1257543590000
            </Text>

            <Text style={screenStyles.dataCardContentLabel}>CPF</Text>
            <Text
              style={{...screenStyles.dataCardContentValue, marginBottom: 12}}>
              419.434.458-85
            </Text>

            <Text style={screenStyles.dataCardContentLabel}>Estado Civil</Text>
            <Text
              style={{...screenStyles.dataCardContentValue, marginBottom: 12}}>
              Solteiro
            </Text>

            <Text style={screenStyles.dataCardContentLabel}>CNS</Text>
            <Text style={screenStyles.dataCardContentValue}>-</Text>
          </View>
          <View style={screenStyles.dataCardFooter}>
            <Text style={screenStyles.dataCardFooterText}>Alterar</Text>
            <ArrowRight style={screenStyles.dataCardFooterIcon} />
          </View>
        </TouchableOpacity>

        <Text style={screenStyles.title}>Contatos</Text>
        <TouchableOpacity
          style={screenStyles.dataCard}
          onPress={() => navigation.navigate('Auth', {screen: 'AddressForm'})}>
          <View style={screenStyles.dataCardContent}>
            <Text style={screenStyles.dataCardContentLabel}>Pessoal</Text>
            <Text style={screenStyles.dataCardContentValue}>
              Telefone: (16) 3017-7275
            </Text>
            <Text
              style={{...screenStyles.dataCardContentValue, marginBottom: 12}}>
              Celular: (16) 99118-5056
            </Text>

            <Text style={screenStyles.dataCardContentLabel}>Urgência</Text>
            <Text style={screenStyles.dataCardContentValue}>
              Telefone: (16) 3017-7275
            </Text>
          </View>
          <View style={screenStyles.dataCardFooter}>
            <Text style={screenStyles.dataCardFooterText}>Alterar</Text>
            <ArrowRight style={screenStyles.dataCardFooterIcon} />
          </View>
        </TouchableOpacity>
      </ScrollView>
    </Container>
  </SafeAreaView>
);

export default Main;
