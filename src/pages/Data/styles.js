import {colors, shadownAndRadius, typography} from '~/assets/styles/index';

export default {
  content: {
    backgroundColor: colors.NORDEN_BLUE,
    flexDirection: 'column',
  },
  mainContent: {
    width: '100%',
  },
  paddingArea: {
    padding: 40,
  },
  title: {
    ...typography.tinyTitle,
    color: colors.NORDEN_WHITE,
    marginBottom: 8,
  },
  dataCard: {
    ...shadownAndRadius.shadow300,
    backgroundColor: colors.NORDEN_LIGHT_BLUE,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: colors.NORDEN_LIGHT_BLUE2,
    borderRadius: 4,
    marginBottom: 30,
  },
  dataCardContent: {
    padding: 20,
    borderBottomWidth: 1,
    borderBottomStyle: 'solid',
    borderBottomColor: colors.NORDEN_LIGHT_BLUE2,
  },
  dataCardContentLabel: {
    ...typography.bodySmallText,
    color: colors.NORDEN_GRAY,
  },
  dataCardContentValue: {
    ...typography.bitTitle,
    color: colors.WHITE,
  },
  dataCardFooter: {
    paddingVertical: 12,
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  dataCardFooterText: {
    ...typography.tagSubtitleText,
    color: colors.NORDEN_WHITE,
  },
  dataCardFooterIcon: {
    color: colors.NORDEN_WHITE,
  },
};
