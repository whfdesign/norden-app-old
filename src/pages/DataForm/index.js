import React, {Component} from 'react';
import {View, SafeAreaView, ScrollView, Text} from 'react-native';
import {Container} from 'native-base';
import {Formik} from 'formik';

import screenStyles from './styles';

import NordenHeader from '~/components/Header';
import NordenLabel from '~/components/Label';
import NordenInput from '~/components/Input';
import NordenButton from '~/components/Button';
import NordenToast from '~/components/Toast';

const DataForm = ({navigation}) => (
  <SafeAreaView style={{flex: 1}}>
    <Container style={screenStyles.content} contentContainerStyle={{flex: 1}}>
      <NordenHeader title="Alterando Dados" navigation={navigation} />
      <NordenToast style={screenStyles.toast} ref={(c) => (this.toast = c)} />
      <ScrollView contentContainerStyle={screenStyles.paddingArea}>
        <Text style={screenStyles.title}>Meus Dados</Text>
        <Formik
          initialValues={{
            address: '',
            file: '',
          }}
          onSubmit={(values, {resetForm}) => {
            resetForm({
              address: '',
              file: '',
            });
          }}>
          {({values, handleChange, handleSubmit, errors}) => (
            <View style={screenStyles.formContainer}>
              <View>
                <NordenLabel
                  text="Beneficiário"
                  style={screenStyles.label}
                  class={errors.address && 'Error'}
                />
                <NordenInput
                  placeholder="Beneficiário"
                  value={values.address}
                  onChangeText={handleChange('address')}
                  autoCompleteType="off"
                  autoCorrect={false}
                  class={errors.address && 'Error'}
                />
                {errors.address && (
                  <NordenLabel class="TagError" text={errors.address} />
                )}

                <NordenLabel
                  text="Data de Nascimento"
                  style={screenStyles.label}
                  class={errors.address && 'Error'}
                />
                <NordenInput
                  placeholder="Data de Nascimento"
                  value={values.address}
                  onChangeText={handleChange('address')}
                  autoCompleteType="off"
                  autoCorrect={false}
                  class={errors.address && 'Error'}
                />
                {errors.address && (
                  <NordenLabel class="TagError" text={errors.address} />
                )}

                <NordenLabel
                  text="CPF"
                  style={screenStyles.label}
                  class={errors.address && 'Error'}
                />
                <NordenInput
                  placeholder="CPF"
                  value={values.address}
                  onChangeText={handleChange('address')}
                  autoCompleteType="off"
                  autoCorrect={false}
                  class={errors.address && 'Error'}
                />
                {errors.address && (
                  <NordenLabel class="TagError" text={errors.address} />
                )}

                <NordenLabel
                  text="Estado civil"
                  style={screenStyles.label}
                  class={errors.address && 'Error'}
                />
                <NordenInput
                  placeholder="Estado civil"
                  value={values.address}
                  onChangeText={handleChange('address')}
                  autoCompleteType="off"
                  autoCorrect={false}
                  class={errors.address && 'Error'}
                />
                {errors.address && (
                  <NordenLabel class="TagError" text={errors.address} />
                )}
              </View>

              <NordenButton
                text="Confirmar"
                class="Accent"
                onPress={handleSubmit}
                style={{marginTop: 30}}
              />
            </View>
          )}
        </Formik>
      </ScrollView>
    </Container>
  </SafeAreaView>
);

export default DataForm;
