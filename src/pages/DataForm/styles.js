import {colors, typography} from '~/assets/styles/index';

export default {
  content: {
    backgroundColor: colors.NORDEN_BLUE,
    flexDirection: 'column',
  },
  mainContent: {
    width: '100%',
  },
  paddingArea: {
    flex: 1,
    padding: 40,
    alignItems: 'stretch',
  },
  title: {
    ...typography.tinyTitle,
    color: colors.NORDEN_WHITE,
    marginBottom: 8,
  },
  text: {
    ...typography.bodySmallText,
    color: colors.NORDEN_WHITE,
    marginBottom: 20,
  },
  label: {
    color: colors.NORDEN_WHITE,
    marginTop: 20,
  },
  toast: {
    marginTop: 68,
  },
  formContainer: {
    flex: 1,
    justifyContent: 'space-between',
  },
  item: {
    backgroundColor: colors.NORDEN_DARK_BLUE,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: colors.NORDEN_LIGHT_BLUE,
    borderRadius: 2,
    paddingVertical: 12,
    paddingHorizontal: 15,
    marginTop: 20,
    marginBottom: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemIcon: {
    color: colors.NORDEN_LIGHT_BLUE,
    marginRight: 10,
  },
  itemText: {
    ...typography.bodyMicroText,
    color: colors.NORDEN_WHITE,
  },
  helpText: {
    ...typography.bodyMicroText,
    color: colors.NORDEN_WHITE,
  }
};
