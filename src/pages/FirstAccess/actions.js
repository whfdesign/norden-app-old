import * as types from './types';
import {navigate} from '~/services/navigator';

export function changeStep(newStep) {
  return {
    type: types.FIRST_ACCESS_CHANGE_STEP,
    newStep: newStep,
  };
}

export function changeDocument(document) {
  return {
    type: types.FIRST_ACCESS_SET_DOCUMENT,
    document: document,
  };
}

export function changePassword(password) {
  return {
    type: types.FIRST_ACCESS_SET_DOCUMENT,
    password: password,
  };
}

export function setToast(success, message) {
  return {
    type: types.FIRST_ACCESS_SET_TOAST,
    toast: {success, message},
  };
}

export function verificarDocumento() {
  return (dispatch, getState) => {
    const {document} = getState().firstAccess;

    return dispatch({
      type: types.FIRST_ACCESS_CHECK_DOCUMENT,
      payload: {
        request: {
          url: '/verificar-documento',
          method: 'POST',
          data: {
            document: document,
          },
        },
      },
    })
      .then(async (result) => {
        let payloadData = result.payload.data;
        dispatch(changeStep('password'));
      })
      .catch((error) => {
        console.log('verificarDocumento - Catch', error);
      });
  };
}

export function primeiroAcesso() {
  return (dispatch, getState) => {
    const {document, password} = getState().firstAccess;

    dispatch(changeStep('concluding'));
    return dispatch({
      type: types.FIRST_ACCESS_CHECK_DOCUMENT,
      payload: {
        request: {
          url: '/primeiro-acesso',
          method: 'POST',
          data: {
            document: document,
            password: password,
          },
        },
      },
    })
      .then(async (result) => {
        let payloadData = result.payload.data;
        navigate('NonAuth', {screen: 'ContractSelector'});
      })
      .catch((error) => {
        console.log('primeiroAcesso - Catch', error);
      });
  };
}
