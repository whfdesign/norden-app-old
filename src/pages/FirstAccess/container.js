import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actions from './actions';
import FirstAccessScreen from './screen';

class IndexContainer extends Component {
  render() {
    return (
      <FirstAccessScreen
        toast={this.props.toast}
        actualStep={this.props.actualStep}
        document={this.props.document}
        password={this.props.password}
        isLoading={this.props.isLoading}
        changeStep={this.props.changeStep}
        changeDocument={this.props.changeDocument}
        changePassword={this.props.changePassword}
        verificarDocumento={this.props.verificarDocumento}
        primeiroAcesso={this.props.primeiroAcesso}
        navigation={this.props.navigation}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  toast: state.firstAccess.toast,
  actualStep: state.firstAccess.actualStep,
  document: state.firstAccess.document,
  password: state.firstAccess.password,
  isLoading: state.firstAccess.isLoading,
});

export default connect(mapStateToProps, actions)(IndexContainer);
