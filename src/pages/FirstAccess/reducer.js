import * as types from './types';

const initialState = {
  isLoading: false,
  actualStep: 'document',
  document: '',
  password: '',
  toast: {},
};

export default function firstAccess(state = initialState, action) {
  if (action.type == types.FIRST_ACCESS_SET_TOAST) {
    return {
      ...state,
      toast: action.toast,
    };
  }

  if (action.type == types.FIRST_ACCESS_CHANGE_STEP) {
    return {
      ...state,
      actualStep: action.newStep,
    };
  }

  if (action.type == types.FIRST_ACCESS_SET_DOCUMENT) {
    return {
      ...state,
      document: action.document,
    };
  }

  if (action.type == types.FIRST_ACCESS_SET_PASSWORD) {
    return {
      ...state,
      password: action.password,
    };
  }

  if (action.type == types.FIRST_ACCESS_CHECK_DOCUMENT) {
    return {
      ...state,
      isLoading: true,
    };
  }

  if (action.type == types.FIRST_ACCESS_CHECK_DOCUMENT_SUCCESS) {
    return {
      ...state,
      isLoading: false,
      actualStep: 'password',
    };
  }

  if (action.type == types.FIRST_ACCESS_CHECK_DOCUMENT_FAIL) {
    return {
      ...state,
      isLoading: false,
    };
  }

  if (action.type == types.FIRST_ACCESS_REGISTER) {
    return {
      ...state,
      isLoading: true,
    };
  }

  if (action.type == types.FIRST_ACCESS_REGISTER_SUCCESS) {
    return {
      ...state,
      isLoading: true,
    };
  }

  if (action.type == types.FIRST_ACCESS_REGISTER_ERROR) {
    return {
      ...state,
      loading: false,
    };
  }

  return state;
}
