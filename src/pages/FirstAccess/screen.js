import React, {Component} from 'react';
import {View, ScrollView, Text} from 'react-native';
import {Container} from 'native-base';

import Spinner from 'react-native-loading-spinner-overlay';

import * as Yup from 'yup';
import {Formik} from 'formik';

import {colors} from '~/assets/styles';
import screenStyles from './styles';

import {validaCpfCnpj} from '~/services/helpers';

import DoctorsTalking from '~/assets/svg/doctors-talking.svg';
import DoctorsTable from '~/assets/svg/doctor-table.svg';

import NordenLabel from '~/components/Label';
import NordenInput from '~/components/Input';
import NordenButton from '~/components/Button';

const documentValidation = Yup.object().shape({
  document: Yup.string()
    .required('Informe um documento')
    .test('is-document', 'Documento inválido', function (value) {
      if (
        typeof value !== 'undefined' &&
        (value.length == 14 || value.length == 18)
      ) {
        return validaCpfCnpj(value);
      }

      return true;
    }),
});

const passwordValidation = Yup.object().shape({
  password: Yup.string()
    .required('Digite uma senha')
    .min(8, 'Use ao menos 8 caracteres'),
});

class FirstAccess extends Component {
  render() {
    return (
      <Container
        style={screenStyles.background}
        contentContainerStyle={screenStyles.mainContent}
        resetScrollToCoords={{x: 0, y: 0}}>
        <Spinner visible={this.props.isLoading} />

        {this.props.actualStep == 'document' && (
          <>
            <Text style={screenStyles.text}>Bem vindo!</Text>
            <Text style={screenStyles.text}>Vamos começar!</Text>
          </>
        )}
        {this.props.actualStep == 'password' && (
          <>
            <Text style={screenStyles.text}>Agore crie uma</Text>
            <Text style={screenStyles.text}>senha de acesso</Text>
          </>
        )}

        {(this.props.actualStep == 'document' ||
          this.props.actualStep == 'password') && (
          <>
            <DoctorsTalking style={screenStyles.center} />
            <ScrollView style={screenStyles.formContainer}>
              {this.props.actualStep == 'document' && (
                <Formik
                  initialValues={{
                    document: this.props.document,
                  }}
                  onSubmit={(values, actions) => {
                    this.props.changeDocument(values.document);
                    this.props.verificarDocumento(this.props.navigation);
                  }}
                  validationSchema={documentValidation}>
                  {({values, handleChange, handleSubmit, errors}) => (
                    <View>
                      <NordenLabel
                        text="Informe um documento"
                        class={errors.document && 'Error'}
                      />
                      <NordenInput
                        placeholder="CPF, CNPJ ou Cartão Norden"
                        value={values.document}
                        onChangeText={handleChange('document')}
                        autoCompleteType="off"
                        autoCorrect={false}
                        keyboardType="numeric"
                        class={errors.document && 'Error'}
                      />
                      {errors.document && (
                        <NordenLabel class="TagError" text={errors.document} />
                      )}
                      <NordenButton
                        text="Continuar"
                        showArrow={true}
                        onPress={handleSubmit}
                        style={{marginTop: 30}}
                      />
                      <NordenButton
                        text="Já tenho cadastro"
                        class="Inline"
                        onPress={() =>
                          this.props.navigation.navigate('NonAuth', {
                            screen: 'Login',
                          })
                        }
                        style={{marginTop: 10}}
                      />
                    </View>
                  )}
                </Formik>
              )}

              {this.props.actualStep == 'password' && (
                <Formik
                  initialValues={{
                    password: '',
                  }}
                  onSubmit={(values, actions) => {
                    this.props.changePassword(values.password);
                    this.props.primeiroAcesso();
                  }}
                  validationSchema={passwordValidation}>
                  {({values, handleChange, handleSubmit, errors}) => (
                    <View>
                      <NordenLabel
                        text="Digite a senha"
                        class={errors.password && 'Error'}
                      />
                      <NordenInput
                        secureTextEntry={true}
                        placeholder="Mínimo de 8 caracteres"
                        value={values.password}
                        onChangeText={handleChange('password')}
                        autoCompleteType="off"
                        autoCorrect={false}
                        class={errors.password && 'Error'}
                      />
                      {errors.password && (
                        <NordenLabel class="TagError" text={errors.password} />
                      )}
                      <NordenButton
                        text="Confirmar"
                        showArrow={true}
                        onPress={handleSubmit}
                        style={{marginTop: 30}}
                      />
                      <NordenButton
                        text="Voltar"
                        class="Inline"
                        onPress={() => this.props.changeStep('document')}
                        style={{marginTop: 10}}
                      />
                    </View>
                  )}
                </Formik>
              )}
            </ScrollView>
          </>
        )}

        {this.props.actualStep == 'concluding' && (
          <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <DoctorsTable />
            <Text style={screenStyles.text}>Concluindo seu cadastro</Text>
            <Spinner size="large" color={colors.NORDEN_LIGHT_BLUE} />
          </View>
        )}
      </Container>
    );
  }
}

export default FirstAccess;
