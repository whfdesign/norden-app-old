import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {View, ScrollView, SafeAreaView, Text} from 'react-native';
import {Container} from 'native-base';

import * as Yup from 'yup';
import {Formik} from 'formik';

import screenStyles from './styles';

import {validaCpfCnpj} from '~/services/helpers';

import DoctorsTable from '~/assets/svg/doctor-table.svg';

import NordenLabel from '~/components/Label';
import NordenInput from '~/components/Input';
import NordenButton from '~/components/Button';
import NordenToast from '~/components/Toast';

import * as ForgotPasswordActions from './actions/index';

const documentValidation = Yup.object().shape({
  document: Yup.string()
    .required('Informe um documento')
    .test('is-document', 'Documento inválido', function (value) {
      if (
        typeof value !== 'undefined' &&
        (value.length == 14 || value.length == 18)
      ) {
        return validaCpfCnpj(value);
      }

      return true;
    }),
});

const ForgotPassword = ({navigation, document, forgotPassword}) => (
  <SafeAreaView style={{flex: 1}}>
    <Container
      style={screenStyles.mainContent}
      contentContainerStyle={{flex: 1}}
      resetScrollToCoords={{x: 0, y: 0}}>
      <NordenToast ref={(c) => (this.toast = c)} />
      <Text style={screenStyles.text}>Tudo bem,</Text>
      <Text style={screenStyles.text}>podemos te ajudar</Text>

      <DoctorsTable style={screenStyles.center} />
      <ScrollView style={screenStyles.formContainer}>
        <Formik
          initialValues={{
            document: '',
          }}
          onSubmit={(values, actions) => {
            this.toast.show('Pronto, verifique seu e-mail.');
            console.log(values, actions);
          }}
          validationSchema={documentValidation}>
          {({values, handleChange, handleSubmit, errors}) => (
            <View>
              <Text style={screenStyles.helperText}>
                Você receberá um e-mail e poderá criar uma nova senha
              </Text>
              <NordenLabel text="Informe um documento" />
              <NordenInput
                placeholder="CPF, CNPJ ou Cartão Norden"
                value={values.document}
                onChangeText={handleChange('document')}
                autoCompleteType="off"
                autoCorrect={false}
                keyboardType="numeric"
              />
              {errors.document && (
                <NordenLabel class="TagError" text={errors.document} />
              )}
              <NordenButton
                text="Enviar e-mail"
                showArrow={true}
                onPress={handleSubmit}
                style={{marginTop: 30}}
              />
              <NordenButton
                text="Voltar"
                class="Inline"
                onPress={() => navigation.navigate('Login')}
                style={{marginTop: 10}}
              />
            </View>
          )}
        </Formik>
      </ScrollView>
    </Container>
  </SafeAreaView>
);

const mapStateToProps = (state) => ({
  document: state.forgotPassword.doument,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ForgotPasswordActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);
