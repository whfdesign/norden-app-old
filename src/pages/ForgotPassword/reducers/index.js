import * as types from '../types';

const initialState = {
  document: '',
};

export default function firstAccess(state = initialState, action) {
  if (action.type == types.FORGOT_PASSWORD_SET_DOCUMENT) {
    return {
      ...state,
      firstAccessData: {
        ...state.firstAccessData,
        document: action.document,
      },
    };
  }

  return state;
}
