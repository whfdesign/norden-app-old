import {colors, typography, shadownAndRadius} from '~/assets/styles/index';

export default {
  mainContent: {
    width: '100%',
    flex: 1,
    alignItems: 'stretch',
    backgroundColor: colors.NORDEN_MEDIUM_GRAY,
    paddingTop: 35,
  },
  innerContainer: {
    width: '100%',
    flex: 1,
  },
  text: {
    ...typography.smallTitle,
    color: colors.NORDEN_LIGHT_BLUE,
    textAlign: 'center',
  },
  center: {
    alignSelf: 'center',
  },
  formContainer: {
    ...shadownAndRadius.shadow100,
    backgroundColor: colors.WHITE,
    borderRadius: 20,
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0,
    paddingTop: 40,
    paddingHorizontal: 48,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: colors.NORDEN_LIGHT_GRAY,
    alignSelf: 'stretch',
  },
  helperText: {
    ...typography.bodySmallText,
    color: colors.NORDEN_LIGHT_BLUE,
    marginBottom: 20
  },
};
