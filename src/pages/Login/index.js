import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {SafeAreaView, ScrollView} from 'react-native';
import {Container} from 'native-base';

import * as Yup from 'yup';
import {Formik} from 'formik';

import {colors} from '~/assets/styles';
import screenStyles from './styles';

import {validaCpfCnpj} from '~/services/helpers';

import DoctorsTalking from '~/assets/svg/doctors-talking.svg';

import NordenLabel from '~/components/Label';
import NordenInput from '~/components/Input';
import NordenButton from '~/components/Button';

import * as LoginActions from './actions/index';
import NordenCheckbox from '~/components/Checkbox';

const formValidation = Yup.object().shape({
  document: Yup.string()
    .required('Informe um documento')
    .test('is-document', 'Documento inválido', function (value) {
      if (
        typeof value !== 'undefined' &&
        (value.length == 14 || value.length == 18)
      ) {
        return validaCpfCnpj(value);
      }

      return true;
    }),
  password: Yup.string()
    .required('Digite uma senha')
    .min(8, 'Use ao menos 8 caracteres'),
});

const Login = ({navigator, navigation, document, password, keepConnected}) => (
  <SafeAreaView style={{flex: 1}}>
    <Container
      style={screenStyles.mainContent}
      contentContainerStyle={{flex: 1, alignItems: 'center'}}
      resetScrollToCoords={{x: 0, y: 0}}>
      <DoctorsTalking style={screenStyles.center} />
      <ScrollView style={screenStyles.formContainer}>
        <Formik
          initialValues={{
            document: '',
            password: '',
          }}
          onSubmit={(values, actions) => {
            console.log(values, actions);
            navigation.navigate('ContractSelector');
          }}
          validationSchema={formValidation}>
          {({
            handleSubmit,
            handleChange,
            handleBlur,
            values,
            errors,
            touched,
          }) => (
            <>
              <NordenLabel
                text="Informe um documento"
                class={touched.document && errors.document && 'Error'}
              />
              <NordenInput
                name="document"
                placeholder="CPF, CNPJ ou Cartão Norden"
                value={values.document}
                onChangeText={handleChange('document')}
                onBlur={handleBlur('document')}
                autoCompleteType="off"
                autoCorrect={false}
                keyboardType="numeric"
                class={touched.document && errors.document && 'Error'}
              />
              {touched.document && errors.document && (
                <NordenLabel class="TagError" text={errors.document} />
              )}

              <NordenLabel
                text="Digite a senha"
                style={{marginTop: 20}}
                class={touched.password && errors.password && 'Error'}
              />
              <NordenInput
                name="password"
                secureTextEntry={true}
                placeholder="Mínimo de 8 caracteres"
                value={values.password}
                onChangeText={handleChange('password')}
                onBlur={handleBlur('password')}
                autoCompleteType="off"
                autoCorrect={false}
                class={touched.password && errors.password && 'Error'}
              />
              {touched.password && errors.password && (
                <NordenLabel class="TagError" text={errors.password} />
              )}
              <NordenCheckbox
                value={keepConnected}
                label="Manter-me conectado"
              />

              <NordenButton
                text="Continuar"
                showArrow={true}
                onPress={handleSubmit}
                style={{marginTop: 30}}
              />
              <NordenButton
                text="Esqueceu sua senha?"
                class="Inline"
                onPress={() => navigation.navigate('ForgotPassword')}
                style={{marginTop: 10}}
              />
            </>
          )}
        </Formik>
      </ScrollView>
    </Container>
  </SafeAreaView>
);

const mapStateToProps = (state) => ({
  document: state.login.document,
  password: state.login.password,
  keepConnected: state.login.keepConnected,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(LoginActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Login);
