import * as types from '../types';

const initialState = {
  document: '',
  password: '',
  keepConnected: true,
};

export default function login(state = initialState, action) {
  if (action.type == types.LOGIN_SET_DATA) {
    return {
      ...state,
      document: action.document,
      password: action.password,
    };
  }

  return state;
}
