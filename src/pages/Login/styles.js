import {colors, typography, shadownAndRadius} from '~/assets/styles/index';

export default {
  mainContent: {
    width: '100%',
    display: 'flex',
    flex: 1,
    backgroundColor: colors.NORDEN_MEDIUM_GRAY,
  },
  innerContainer: {
    width: '100%',
    flex: 1,
  },
  text: {
    ...typography.smallTitle,
    color: colors.NORDEN_LIGHT_BLUE,
    textAlign: 'center',
  },
  center: {
    alignSelf: 'center',
  },
  formContainer: {
    ...shadownAndRadius.shadow100,
    backgroundColor: colors.WHITE,
    borderRadius: 20,
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0,
    paddingTop: 40,
    paddingHorizontal: 48,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: colors.NORDEN_LIGHT_GRAY,
    flex: 1,
    alignSelf: 'stretch',
    paddingBottom: 20,
  },
};
