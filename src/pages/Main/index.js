import React, {Component} from 'react';
import {View, SafeAreaView, ScrollView, Text} from 'react-native';
import {Container} from 'native-base';

import screenStyles from './styles';
import NordenHeader from '~/components/Header';
import NordenCard from '~/components/Card';
import NordenLack from '~/components/Lack';

const Main = ({navigation}) => (
  <SafeAreaView style={{flex: 1}}>
    <Container
      scrollEnabled={false}
      style={screenStyles.content}
      contentContainerStyle={{flex: 1, alignItems: 'center'}}
      resetScrollToCoords={{x: 0, y: 0}}>
      <NordenHeader navigation={navigation} />
      <ScrollView style={screenStyles.scrollView}>
        <View style={screenStyles.mainContent}>
          <View style={screenStyles.textContent}>
            <Text style={screenStyles.text}>Apresente seu</Text>
            <Text style={screenStyles.text}>cartão virtual</Text>
          </View>
          <NordenCard style={screenStyles.card} />
        </View>
        <NordenLack
          // lacks={[]}
          lacks={[
            {name: 'Cirurgias Simples', days: 120},
            {name: 'Internações', days: 120},
          ]}
          navigation={navigation}
        />
      </ScrollView>
    </Container>
  </SafeAreaView>
);

export default Main;
