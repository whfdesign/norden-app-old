import { Dimensions } from 'react-native';
import {colors, typography, shadownAndRadius} from '~/assets/styles/index';

export default {
  content: {
    backgroundColor: colors.WHITE,
  },
  scrollView: {
    width: '100%',
  },
  mainContent: {
    width: '100%',
    height: 200,
    backgroundColor: colors.NORDEN_MEDIUM_BLUE,
    marginBottom: 120,
  },
  textContent: {
    paddingTop: 55,
    paddingBottom: 15,
    paddingHorizontal: 20,
  },
  text: {
    ...typography.tinyTitle,
    color: colors.NORDEN_WHITE,
  },
  card: {
    marginHorizontal: 20,
  },
};
