import React, {Component} from 'react';
import {View, SafeAreaView, ScrollView, Text} from 'react-native';
import {Container} from 'native-base';

import screenStyles from './styles';

import ArrowRight from '~/assets/svg/arrow-right.svg';

import NordenHeader from '~/components/Header';

const PrivacyPolicy = ({navigation}) => (
  <SafeAreaView style={{flex: 1}}>
    <Container
      scrollEnabled={true}
      contentContainerStyle={screenStyles.content}>
      <NordenHeader title="Política de Privacidade" navigation={navigation} />
      <ScrollView contentContainerStyle={screenStyles.paddingArea}>
        {[0, 1, 2, 3, 4, 5, 6].map((i) => (
          <>
            <Text style={screenStyles.title}>Título Aqui</Text>
            <Text style={screenStyles.text}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat.{' '}
            </Text>
          </>
        ))}
      </ScrollView>
    </Container>
  </SafeAreaView>
);

export default PrivacyPolicy;
