import {colors, shadownAndRadius, typography} from '~/assets/styles/index';

export default {
  content: {
    backgroundColor: colors.WHITE,
    flexDirection: 'column',
    flex: 1,
    alignItems: 'stretch',
  },
  mainContent: {
    width: '100%',
  },
  paddingArea: {
    padding: 20,
  },
  title: {
    ...typography.tagSubtitleText,
    color: colors.NORDEN_DARK_BLUE,
    marginBottom: 12,
  },
  text: {
    ...typography.bodySmallText,
    color: colors.NORDEN_LIGHT_BLUE,
    marginBottom: 20,
  },
};
