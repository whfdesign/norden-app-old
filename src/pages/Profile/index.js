import React, {Component} from 'react';
import {
  View,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import {Container} from 'native-base';

import screenStyles from './styles';

import Home from '~/assets/svg/home.svg';
import User from '~/assets/svg/user.svg';

import Swap from '~/assets/svg/swap.svg';

import Lock from '~/assets/svg/lock.svg';
import Logout from '~/assets/svg/logout.svg';

import NordenHeader from '~/components/Header';

const Profile = ({navigation}) => (
  <SafeAreaView style={{flex: 1}}>
    <Container style={screenStyles.content} contentContainerStyle={{flex: 1}}>
      <NordenHeader title="Meu perfil" navigation={navigation} />
      <ScrollView style={screenStyles.paddingArea}>
        <View style={screenStyles.profileArea}>
          <View style={screenStyles.profileImage}>
            <Image source={require('~/assets/images/user.png')} />
          </View>
          <View>
            <Text style={screenStyles.profileName}>Marlon Trovão</Text>
            <Text style={screenStyles.profilePlan}>
              Plano integrado Referência
            </Text>
          </View>
        </View>
        <Text style={screenStyles.title}>Dados Pessoais</Text>
        <TouchableOpacity
          style={screenStyles.item}
          onPress={() => navigation.navigate('Auth', {screen: 'Address'})}>
          <Home style={screenStyles.itemIcon} />
          <Text style={screenStyles.itemText}>Meu Endereço</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={screenStyles.item}
          onPress={() => navigation.navigate('Auth', {screen: 'Data'})}>
          <User style={screenStyles.itemIcon} />
          <Text style={screenStyles.itemText}>Meus Dados </Text>
        </TouchableOpacity>

        {/*<Text style={screenStyles.title}>Plano</Text>*/}
        {/*<TouchableOpacity style={screenStyles.item}>*/}
        {/*  <Swap style={screenStyles.itemIcon} />*/}
        {/*  <Text style={screenStyles.itemText}>Meus Contratos</Text>*/}
        {/*</TouchableOpacity>*/}
        {/*<TouchableOpacity style={screenStyles.item}>*/}
        {/*  <User style={screenStyles.itemIcon} />*/}
        {/*  <Text style={screenStyles.itemText}>Meus Dependentes</Text>*/}
        {/*</TouchableOpacity>*/}

        <Text style={screenStyles.title}>Segurança</Text>
        <TouchableOpacity
          style={screenStyles.item}
          onPress={() =>
            navigation.navigate('Auth', {screen: 'ChangePassword'})
          }>
          <Lock style={screenStyles.itemIcon} />
          <Text style={screenStyles.itemText}>Mudar Senha de Acesso</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={screenStyles.item}
          onPress={() => navigation.navigate('NonAuth', {screen: 'Login'})}>
          <Logout style={screenStyles.itemIcon} />
          <Text style={screenStyles.itemText}>Sair do App</Text>
        </TouchableOpacity>
      </ScrollView>
    </Container>
  </SafeAreaView>
);

export default Profile;
