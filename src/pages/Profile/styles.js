import {colors, typography} from '~/assets/styles/index';

export default {
  content: {
    backgroundColor: colors.NORDEN_BLUE,
    flexDirection: 'column',
  },
  mainContent: {
    width: '100%',
  },
  paddingArea: {
    flex: 1,
    paddingVertical: 40,
    paddingHorizontal: 50,
    paddingBottom: 220
  },
  profileArea: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  profileImage: {
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: colors.NORDEN_MEDIUM_GRAY,
    borderRadius: 44,
    marginRight: 12,
  },
  profileName: {
    ...typography.bodyNormalText,
    color: colors.WHITE,
  },
  profilePlan: {
    ...typography.bodySmallText,
    color: colors.NORDEN_GRAY,
  },
  title: {
    ...typography.tagSubtitleText,
    color: colors.NORDEN_LIGHT_BLUE,
    marginTop: 30,
  },
  item: {
    backgroundColor: colors.NORDEN_DARK_BLUE,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: colors.NORDEN_LIGHT_BLUE,
    borderRadius: 2,
    paddingVertical: 12,
    paddingHorizontal: 15,
    marginTop: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemIcon: {
    color: colors.NORDEN_LIGHT_BLUE,
    marginRight: 10,
  },
  itemText: {
    ...typography.bodyMicroText,
    color: colors.NORDEN_WHITE,
  },
};
