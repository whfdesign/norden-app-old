import React, {Component} from 'react';
import {View, SafeAreaView, ScrollView, Text} from 'react-native';
import {Container, List, ListItem} from 'native-base';
import Svg, {Circle} from 'react-native-svg';
import {colors} from '~/assets/styles';

import screenStyles from './styles';

import ArrowRight from '~/assets/svg/arrow-right.svg';

import NordenHeader from '~/components/Header';


const ShortageHistory = ({navigation}) => (
  <SafeAreaView style={{flex: 1}}>
    <Container
      scrollEnabled={true}
      contentContainerStyle={screenStyles.content}>
      <NordenHeader title="Histórico de Carência" navigation={navigation} />
      <ScrollView style={screenStyles.paddingArea}>
        <Text style={screenStyles.title}>Serviços e Prazos</Text>
        <Text style={screenStyles.start}>Plano ativado em: 01/09/2020.</Text>

        <List style={screenStyles.list}>
          {[0, 1, 2, 3, 4, 5, 6].map((i) => (
            <ListItem style={screenStyles.listItem} key={i}>
              <View style={screenStyles.listItemTimeline}>
                <Svg height="20" width="20">
                  <Circle
                    cx="9"
                    cy="9"
                    r="9"
                    fill={colors.NORDEN_MEDIUM_GRAY}
                  />
                </Svg>
              </View>
              {i < 2 ? (
                <View style={screenStyles.listItemView}>
                  <View style={screenStyles.listItemViewContent}>
                    <Text style={screenStyles.listItemTitle}>
                      Assistente Personal Saúde On-line
                    </Text>
                    <View style={screenStyles.listItemBudgetSuccess}>
                      <Text style={screenStyles.listItemText}>DISPONÍVEL</Text>
                    </View>
                  </View>
                </View>
              ) : (
                <View style={screenStyles.listItemView}>
                  <View style={screenStyles.listItemViewContent}>
                    <Text style={screenStyles.listItemTitle}>
                      Assistente Personal Saúde On-line
                    </Text>
                    <View style={screenStyles.listItemBudget}>
                      <Text style={screenStyles.listItemText}>EXPIRADA</Text>
                    </View>
                  </View>
                  <Text style={screenStyles.shortageInfo}>
                    Este é tempo que falta para ter acesso ao P.A. do Norden
                    Hospital para urgências ou emergências. Não deve ser usado
                    para fim de tratamento.
                  </Text>
                </View>
              )}
            </ListItem>
          ))}
        </List>
      </ScrollView>
    </Container>
  </SafeAreaView>
);

export default ShortageHistory;
