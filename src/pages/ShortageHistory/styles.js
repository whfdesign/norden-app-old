import {colors, shadownAndRadius, typography} from '~/assets/styles/index';

export default {
  content: {
    backgroundColor: colors.WHITE,
    flexDirection: 'column',
    flex: 1,
    alignItems: 'stretch',
  },
  mainContent: {
    width: '100%',
  },
  paddingArea: {
    flex: 1,
    paddingRight: 20,
  },
  list: {
    paddingBottom: 40,
  },
  title: {
    ...typography.tinyTitle,
    color: colors.NORDEN_BLACK,
    marginTop: 40,
    marginLeft: 60,
  },
  start: {
    ...typography.bodyMicroText,
    color: colors.NORDEN_LIGHT_BLUE,
    marginBottom: 7,
    marginLeft: 60,
  },
  text: {
    ...typography.bodySmallText,
    color: colors.NORDEN_LIGHT_BLUE,
    marginBottom: 20,
  },
  listItem: {
    borderBottomWidth: 0,
    paddingVertical: 12,
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  listItemTimeline: {
    paddingBottom: 12,
    paddingLeft: 8,
    paddingRight: 24,
    paddingTop: 20,
  },
  listItemView: {
    flex: 1,
    borderBottomWidth: 1,
    borderBottomStyle: 'solid',
    borderBottomColor: colors.NORDEN_LIGHT_GRAY,
    paddingBottom: 12,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  listItemViewContent: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 12,
  },
  listItemTitle: {
    ...typography.tagSubtitleText,
    color: colors.NORDEN_DARK_BLUE,
    flex: 1,
  },
  listItemText: {
    ...typography.bodySmallText,
    color: colors.NORDEN_BLUE,
  },
  listItemBudget: {
    backgroundColor: colors.NORDEN_MEDIUM_GRAY,
    borderRadius: 12,
    paddingVertical: 5,
    paddingHorizontal: 8,
    marginLeft: 15,
  },
  listItemBudgetSuccess: {
    backgroundColor: colors.NORDEN_SUCCESS,
    borderRadius: 12,
    paddingVertical: 5,
    paddingHorizontal: 8,
    marginLeft: 15,
  },
  shortageInfo: {
    ...typography.bodySmallText,
    color: colors.NORDEN_LIGHT_BLUE,
    marginBottom: 20,
  },
};
