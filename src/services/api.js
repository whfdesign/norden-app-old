import axios from 'axios';

const api = axios.create({
  baseURL: 'https://demo9839409.mockable.io',
});

export default api;
