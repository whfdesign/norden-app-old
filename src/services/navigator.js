let instance;

function setContainer(container) {
  if (container && !instance) {
    instance = container;
    console.log('[NavigatorService] Instance setted!');
  }
}

function navigate(routeName, params) {
  return instance.navigate(routeName, params);
}

export default {
  setContainer,
  navigate,
};

export {setContainer, navigate};
