let instance;

function setContainer(container) {
  if (container && !instance) {
    instance = container;
    console.log('[ToastService] Instance setted!');
  }
}

function show(message, type = 'success') {
  return instance.show(message, type);
}

export default {
  setContainer,
  show,
};

export {setContainer, show};
